// on Client handle "error": "Response not successful: Received status code 400"

const formatError = (error) => {

    const {message} = error;
    
    if (message.includes('##authentication #token')) return new Error('error authentication token missing');
    if (message.includes('##authentication #user')) return new Error('error authentication token invalid');
    if (message.includes('#authentication')) return new Error('error authentication failed');

    if (message.includes('#find #item')) return new Error('error find item failed');
    if (message.includes('#create #item')) return new Error('error create item failed');
    if (message.includes('#update #item')) return new Error('error update item failed');
    if (message.includes('#delete #item')) return new Error('error delete item failed');

    if (message.includes('#find #hook')) return new Error('error find hook failed');
    if (message.includes('#remove #hook')) return new Error('error remove hook failed');
    if (message.includes('#display #hook')) return new Error('error display hook failed');
    if (message.includes('#organize #hook')) return new Error('error organize hook failed');
    if (message.includes('#hook #hook')) return new Error('error hook hook failed');
    if (message.includes('#move #hook')) return new Error('error move hook failed');
    if (message.includes('#copy #hook')) return new Error('error copy hook failed');

    if (message.includes('#list #book #item')) return new Error('error list book item failed');
    if (message.includes('#find #book #item')) return new Error('error find book item failed');
    if (message.includes('#create #book #item')) return new Error('error create book item failed');
    if (message.includes('#update #book #item')) return new Error('error update book item failed');
    if (message.includes('#delete #book #item')) return new Error('error delete book item failed');
    
    return new Error('error something failed');

};
export default formatError;