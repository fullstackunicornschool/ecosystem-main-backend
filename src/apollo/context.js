// AUTHENTICATION ENABLED -------------------

import Prisma from "@prisma/client";
import use from "../use/index.js";
import gql from "graphql-tag";

const prisma = new Prisma.PrismaClient();

const requestIdentification = (queryReq = null) => {
	try {
		const reqObj = gql`
			${queryReq}
		`;
		const res = reqObj.definitions.filter(
			(definition) => definition.kind === "OperationDefinition"
		);
		return {
			reqType: res[0]?.operation,
			reqName: res[0]?.selectionSet?.selections[0]?.name?.value,
		};
	} catch (error) {
		use.throwError.catch(`Context #requestIdentification | message: ${error.message}`);
	}
};

const context = async ({ req }) => {
	try {
		const publicFetches = ["fetchElement"];
		const { reqType, reqName } = requestIdentification(req?.body?.query);
		let user = {};
		if (reqType !== "query" || (reqType === "query" && !publicFetches.includes(reqName))) {
			const { authorization } = req.headers;
			const token =
				typeof authorization === "string" && authorization.startsWith("Bearer ")
					? authorization.replace("Bearer ", "")
					: false;
			if (!token) use.throwError.authentication(`#authentication #token`);
			user = await use.auth.user(token, reqType);
			if (!user) use.throwError.authentication(`#authentication #user`);
		}
		return {
			prisma,
			...user,
		};
	} catch (error) {
		use.throwError.catch(`Context #authentication | message: ${error.message}`);
	}
};
export default context;

// AUTHENTICATION DISABLED ------------------

// import Prisma from "@prisma/client";
// import use from "../use/index.js";
// import gql from "graphql-tag";

// const prisma = new Prisma.PrismaClient();

// const context = async ({ req }) => {
// 	try {
// 		return {
// 			prisma,
// 			user: {
// 				id: "USER-12345",
// 			},
// 		};
// 	} catch (error) {
// 		use.throwError.catch(`Context #authentication | message: ${error.message}`);
// 	}
// };
// export default context;
