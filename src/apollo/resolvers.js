const resolvers = {
	// list and find has been replaced with fetch.
	// it should be done with dataSource too.

	Query: {
		fetchProtectedElement: async (_, input, context) => {
			return await context.dataSources.element.findProtected(input, context);
		},
		fetchElement: async (_, input, context) => {
			return await context.dataSources.element.find(input, context);
		},
	},

	Mutation: {
		createElement: async (_, input, context) => {
			return await context.dataSources.element.create(input, context);
		},
	},
};
export default resolvers;
