import { gql } from "apollo-server";

const typeDefs = gql`
	# --------- Enum ---------
	enum type {
		text
		picture
	}

	# --------- Element ---------
	type Element {
		id: ID!
		author_id: String!
		protected: Boolean
		value: String
		type: String
		created: String
		updated: String
		normalized: Boolean
	}

	# ---------- INPUT --------------

	input id {
		id: ID!
	}

	input ids {
		ids: [ID!]!
	}

	input list {
		id: ID!
		order_by: String = "id"
		order: String = "asc"
	}

	input newElement {
		protected: Boolean
		value: String!
		type: type
	}

	# ---------- QUERY --------------
	type Query {
		fetchElement(input: id!): Element
		fetchProtectedElement(input: id!): Element
	}

	# ---------- MUTATION -----------
	type Mutation {
		createElement(input: newElement!): Element
	}
`;
export default typeDefs;
