// import requests
import Find from "./Find.js";
import Create from "./Create.js";

const request = {
	find: new Find(),
	create: new Create(),
};
export default request;
