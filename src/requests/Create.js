export default class Create {
	item(input, author_id, itemName, prisma) {
		return prisma[itemName].create({
			data: {
				author_id,
				...input,
			},
		});
	}
}
