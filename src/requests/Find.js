export default class Find {
	item(id, itemName, prisma) {
		return prisma[itemName].findFirst({
			where: { id, protected: false },
		});
	}
	protectedItem(id, itemName, prisma, author_id) {
		return prisma[itemName].findFirst({
			where: { id, author_id, protected: true },
		});
	}
}
