// AUTHENTICATION ENABLED -------------------

import Auth from "./Auth.js";
import Normalize from "./Normalize.js";
import ThrowError from "./ThrowError.js";

const use = {
	auth: new Auth(),
	normalize: new Normalize(),
	throwError: new ThrowError(),
};
export default use;

// AUTHENTICATION DISABLED ------------------

// import Normalize from "./Normalize.js";
// import ThrowError from "./ThrowError.js";

// const use = {
// 	normalize: new Normalize(),
// 	throwError: new ThrowError(),
// };
// export default use;
