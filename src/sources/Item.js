import { DataSource } from "apollo-datasource";
import request from "../requests/index.js";
import use from "../use/index.js";

export default class Item extends DataSource {
	constructor() {
		super();
		this.itemName = "element";
		this.includeFind = {};

		this.request = {
			list: async () => {},
			findProtected: request.find.protectedItem,
			find: request.find.item,
			create: request.create.item,
		};
	}

	initialize(config) {
		this.context = config.context;
	}

	async findProtected({ input }, { prisma, user }) {
		const { id } = input;
		try {
			return await this.request.findProtected(id, this.itemName, prisma, user.id);
		} catch (error) {
			use.throwError.catch(`#findProtected #item ${this.itemName} | message: ${error.message}`);
		}
	}

	async find({ input }, { prisma }) {
		const { id } = input;
		try {
			return await this.request.find(id, this.itemName, prisma);
		} catch (error) {
			use.throwError.catch(`#find #item ${this.itemName} | message: ${error.message}`);
		}
	}

	async create({ input }, { prisma, user }) {
		try {
			const create = this.request.create(input, user.id, this.itemName, prisma);
			const result = await create;
			return use.normalize.createResponse(result);
		} catch (error) {
			use.throwError.catch(`#create #item ${this.itemName} | message: ${error.message}`);
		}
	}
}
